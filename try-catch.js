const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
  ];




const requiredValues = ['author', 'name', 'price'];
// функция для проверки наличия обязательных свойств
function hasRequiredProperties(booksList, requiredProperties) {
  booksList.forEach((book) => {
    try{
      const emptyProp = requiredProperties.find(prop => !Object.keys(book).includes(prop));
      if (emptyProp) {
            throw new Error(`Book is missing required property: ${emptyProp}`);
          }
      createBookListItem(book);
    } catch(error) {
      console.log(error.name);
      console.log(error.message);
    }
  });
  return requiredProperties.every(prop => prop in booksList);
}


// функция для создания HTML li элементов
function createBookListItem(book) {
  const html = Object.entries(book)
    // .filter(([key]) => propertiesToDisplay.includes(key))
    .map(([key, value]) => `<li>${key}: ${value}</li>`)
    .join('');
  // return `<li>${html}</li>`;
  return document.getElementById('root').insertAdjacentHTML('beforeend', `<ul>${html}</ul>`);
}

hasRequiredProperties(books, requiredValues);




// document.getElementById('root').insertAdjacentHTML('beforeend', `<ul>${bookListHtml}</ul>`);






// Код без метода try catch рабочий, базовый, с него делала код итоговый на проверку
// // функция для проверки наличия обязательных свойств
// function hasRequiredProperties(book) {
//   const requiredProperties = ['author', 'name', 'price'];
//   return requiredProperties.every(prop => prop in book);
// }


// // функция для создания HTML li элементов
// function createBookListItem(book) {
//   // const propertiesToDisplay = ['author', 'name', 'price'];
//   const html = Object.entries(book)
//     // .filter(([key]) => propertiesToDisplay.includes(key))
//     .map(([key, value]) => `<div>${key}: ${value}</div>`)
//     .join('');
//   return `<li>${html}</li>`;
// }

// // создаем массив только с книгами, у которых есть все обязательные свойства
// const validBooks = books.filter(hasRequiredProperties);
// console.log(validBooks);

// // создаем HTML список книг
// const bookListHtml = validBooks.map(createBookListItem).join('');

// // вставляем HTML список в элемент с id="root"
// document.getElementById('root').insertAdjacentHTML('beforeend', `<ul>${bookListHtml}</ul>`);









// Вариант который сдала первый раз (рабочий, но не универсальный)
// const root = document.getElementById("root");
// const ul = document.createElement("ul");

// books.forEach(book => {
//     try {
//         if(book.author && book.name && book.price) {
//             const li = document.createElement("li");
//             li.textContent = `Author: ${book.author}, Book name: ${book.name}, Price: ${book.price} uah`;
//             ul.appendChild(li);
//         } else {
//             throw new Error(`У книги "${book.name}" не вистачає властивості: ${!book.author ? 'author' : ''} ${!book.name ? 'name' : ''} ${!book.price ? 'price' : ''}`);
//         }
//     } catch (error) {
//         console.log(error.name);
//         console.log(error.message);
//     }
// });

// root.appendChild(ul);